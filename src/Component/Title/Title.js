import React from 'react';
import './Title.css';
import TitleBold from './TitleBold/TitleBold';
import TitleText from './TitleText/TitleText';

class Title extends React.Component {

  state = {
    TextBolder: [
      {
        first: 'Reliable,efficient delivery',
        second: 'Powered By Technology',
        thirth: 'our Artificial intelligence powered tools use millions of project data points to ensure that your project is successful '
      },
    ],
  }
  render() {
    return(this.state.TextBolder.map((item)=>{

          return (

      <div className='bold'><TitleBold textBolder={item.first}
       />
       <TitleBold textBold={item.second}/>
       <TitleText text={item.thirth}/>
       </div>


    )
    }))




  }


}

export default Title;
