import React from 'react';
import './Items.css';
import Calculator from './Calculator/Calculator';
import Karma from './Karma/Karma';
import Supervisor from './Supervisor/Supervisor';
import TeamBuilder from './TeamBuilder/TeamBuilder';

class Items extends React.Component{
  state={
    product:[
      {title0:'Team Builder',
        sen0:"Scans our talent network to create the optimal team for your project",
        title1:'supervisor',
        sen1:"Monitors actitvity to identify project roadblocks",
        title2:'calculator',
        sen2:"Uses data from past projects to provide better delivery estimates",
        title3:'Karma',
        sen3:"Regularly evaluates talent to our ensure quality"},
    ]
  }
  render(){
    return(this.state.product.map((item)=>{
        return(
      <div className='items'> 
      <div className='both2'><TeamBuilder title={item.title0}
                                          sen={item.sen0}/>
                           
                               
                                
                                </div>
         
         <div className='both'>
             <Supervisor title={item.title1}
                                          sen={item.sen1}/> 
             <Calculator title={item.title2}
                                          sen={item.sen2}/>
       
            </div>
       
            <div className='both2'>  <Karma title={item.title3}
                                          sen={item.sen3}/></div>
   
      </div>
    )
    }))
  
    
  }
}



export default Items;
