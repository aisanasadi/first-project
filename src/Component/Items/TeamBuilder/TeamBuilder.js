import React from "react";
import Team from'./icon-team-builder.svg'
import "./TeamBuilder.css"

const TeamBuilder=(props)=>{

    return(   
        <div className="item">
    <div className="team">
       <p>{ props.title}</p>
        <h3>{props.sen}</h3>
        <div><img src={Team}/></div>
    </div>
</div>
)
 


}

export default TeamBuilder