import React from "react";
import Super from './icon-supervisor.svg'
import "./Supervisor.css"

const Supervisor=(props)=>{

    return(   <div className="item">
        <div className="super">
        <p>{ props.title}</p>
        <h3>{props.sen}</h3>
        <div><img src={Super}/></div>
    </div>
</div>
)
 


}

export default Supervisor