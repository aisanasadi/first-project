import React from 'react';
import './Main.css';
import Title from '../Component/Title/Title';
import Items from '../Component/Items/Items';

class Main extends React.Component{
  render(){
    return(
      <div>
      <Title/>  
       <Items/>
       
      </div>
    )
    
  }
}



export default Main;
